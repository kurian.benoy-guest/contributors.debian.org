from django.test import TestCase
from sources.test_common import *
from dc.unittest import SourceFixtureMixin


class MIATestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1", "alioth", "alioth1", None:
            cls._add_method(cls._test_success, user)

    def _test_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("mia_last_significant"))
            res = response.json()
