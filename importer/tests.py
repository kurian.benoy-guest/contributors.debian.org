from django.test import TestCase, RequestFactory
from django.core.urlresolvers import reverse
from contributors import models as bmodels
from . import importer
from io import BytesIO
import datetime
import json

class ImportTest(TestCase):
    def setUp(self):
        self.source = bmodels.Source(name="test", auth_token="foo")
        self.source.save()

        self.ct_upload = bmodels.ContributionType(
            source=self.source,
            name="upload",
        )
        self.ct_upload.save()

        self.ct_sponsor = bmodels.ContributionType(
            source=self.source,
            name="sponsor",
        )
        self.ct_sponsor.save()

        self.ct_review = bmodels.ContributionType(
            source=self.source,
            name="review",
        )
        self.ct_review.save()

        self.import_data1 = {
            "id": [{
                "type": "login",
                "id": "enrico",
            }],
            "contributions": [
                {
                    "type": "upload",
                    "begin": "2013-08-11",
                    "end": "2013-08-18",
                    "url": "http://example.org/foo/enrico",
                }
            ],
        }

    def make_request(self, data):
        factory = RequestFactory()
        submission = BytesIO(json.dumps(data).encode("utf-8"))
        submission.name = "submission.json"
        return factory.post(reverse("contributors_post"), data={
            "source": self.source.name,
            "auth_token": self.source.auth_token,
            "data": submission,
        })

    def test_simple_import(self):
        """
        Test a simple import run
        """
        req = self.make_request([self.import_data1])

        i = importer.Importer()
        self.assertTrue(i.import_request(req))
        self.assertEqual(i.results.code, 200)
        self.assertEqual(i.results.identifiers_skipped, 0)
        self.assertEqual(i.results.contributions_processed, 1)
        self.assertEqual(i.results.contributions_created, 1)
        self.assertEqual(i.results.contributions_updated, 0)
        self.assertEqual(i.results.contributions_skipped, 0)
        self.assertEqual(i.results.records_parsed, 1)
        self.assertEqual(i.results.records_skipped, 0)
        self.assertEqual(i.results.errors, [])

        # See that the identifier was created
        identifier = bmodels.Identifier.objects.get(type="login", name="enrico")
        self.assertEqual(identifier.hidden, False)

        # See that the contribution was created
        contrib = bmodels.Contribution.objects.get(identifier=identifier, type=self.ct_upload)
        self.assertEqual(contrib.begin, datetime.date(2013, 8, 11))
        self.assertEqual(contrib.until, datetime.date(2013, 8, 18))
        self.assertEqual(contrib.url, "http://example.org/foo/enrico")

    def test_empty_import(self):
        req = self.make_request([])
        i = importer.Importer()
        self.assertTrue(i.import_request(req))
        self.assertEqual(i.results.code, 200)
        self.assertEqual(i.results.identifiers_skipped, 0)
        self.assertEqual(i.results.contributions_processed, 0)
        self.assertEqual(i.results.contributions_created, 0)
        self.assertEqual(i.results.contributions_updated, 0)
        self.assertEqual(i.results.contributions_skipped, 0)
        self.assertEqual(i.results.records_parsed, 0)
        self.assertEqual(i.results.records_skipped, 0)

    def test_failed_imports(self):
        """
        Test a simple import run
        """
        req = self.make_request([self.import_data1])
        req.POST["source"] = "fail"
        i = importer.Importer()
        self.assertFalse(i.import_request(req))
        self.assertEqual(i.results.code, 404)

        req = self.make_request([self.import_data1])
        req.POST["auth_token"] = "fail"
        i = importer.Importer()
        self.assertFalse(i.import_request(req))
        self.assertEqual(i.results.code, 403)

class SourceImportTest(TestCase):
    def test_empty_import(self):
        bmodels.Source.import_json([])

    def test_simple_import(self):
        source = bmodels.Source(name="www", desc="www.debian.org", url="http://www.debian.org", auth_token="12345")
        source.save()
        contrib = bmodels.ContributionType(
            source=source,
            name="commit",
            desc="www.debian.org webml commits",
            contrib_desc="Website committer")
        contrib.save()

        sources = bmodels.Source.export_json(with_tokens=True)
        source.delete()
        bmodels.Source.import_json(sources)

        source = bmodels.Source.objects.get(name="www")
        self.assertEqual(source.name, "www")
        self.assertEqual(source.desc, "www.debian.org")
        self.assertEqual(source.url, "http://www.debian.org")
        self.assertEqual(source.auth_token, "12345")
        contrib = source.contribution_types.all()[0]
        self.assertEqual(contrib.name, "commit")
        self.assertEqual(contrib.desc, "www.debian.org webml commits")
        self.assertEqual(contrib.contrib_desc, "Website committer")
