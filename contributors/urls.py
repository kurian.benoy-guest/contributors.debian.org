from django.conf.urls import url
from importer import views as iviews
from . import views

urlpatterns = [
    url(r'^year/(?P<year>\d+)$', views.Contributors.as_view(), name="contributors_year"),
    url(r'^post$', iviews.PostSourceView.as_view(), name="contributors_post"),
    url(r'^test_post$', iviews.TestPostSourceView.as_view(), name="contributors_test_post"),
    url(r'^flat$', views.ContributorsFlat.as_view(), name="contributors_flat"),
    url(r'^new$', views.ContributorsNew.as_view(), name="contributors_new"),
    url(r'^mia$', views.ContributorsMIA.as_view(), name="contributors_mia"),
    url(r'^mia/query$', views.MIAQuery.as_view(), name="contributors_mia_query"),
    url(r'^sources/flat$', views.SourcesFlat.as_view(), name="contributors_sources_flat"),
    url(r'^export/sources$', iviews.ExportSources.as_view(), name="contributors_export_sources"),
    url(r'^site_status$', views.SiteStatus.as_view(), name="site_status"),
    url(r'^claim/$', views.Claim.as_view(), name="contributors_claim"),
    url(r'^claim/identifiers/(?P<type>[a-z]+)/$', views.ClaimIdents.as_view(), name="contributors_claim_idents"),
    url(r'^claim/people/$', views.ClaimPeople.as_view(), name="contributors_claim_people"),
]
