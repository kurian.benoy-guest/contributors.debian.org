from django.test import TestCase
from sources.test_common import *
from dc.unittest import SourceFixtureMixin
import datetime
import json

class ContributorsTestCase(SimpleSourceFixtureMixin, DCTestUtilsMixin, TestCase):
    def test_contributors(self):
        tc = DCTestClient(self, 'contributors')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_contributors_flat(self):
        tc = DCTestClient(self, 'contributors_flat')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_contributors_new(self):
        tc = DCTestClient(self, 'contributors_new')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_site_status(self):
        tc = DCTestClient(self, 'site_status')
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(Success(), user)

    def test_export_sources(self):
        tc = DCTestClient(self, 'contributors_export_sources')

        class CanSeeTokens(DCTestClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEqual(tc.response.status_code, 200)
                data = tc.response.json()
                source = [x for x in data if x["name"] == "test"][0]
                tc.fixture.assertIsNotNone(source)
                tc.fixture.assertEqual(source["auth_token"], "testsecret")

        class CannotSeeTokens(DCTestClientCheck):
            def check_result(self, tc):
                tc.fixture.assertEqual(tc.response.status_code, 200)
                data = tc.response.json()
                source = [x for x in data if x["name"] == "test"][0]
                tc.fixture.assertIsNotNone(source)
                tc.fixture.assertNotEqual(source["auth_token"], "testsecret")

        tc = DCTestClient(self, 'contributors_export_sources')
        tc.assertGet(CanSeeTokens(), self.user_admin)
        for user in (self.user_dd, self.user_alioth, None):
            tc.assertGet(CannotSeeTokens(), user)

        # Even members of a data source cannot see tokens in export, not even
        # in the sources they admin, to avoid accidentally leaking tokens when
        # giving an export to new developers.
        self.source.admins.add(self.user_dd)
        self.source.admins.add(self.user_alioth)
        for user in (self.user_dd, self.user_alioth, None):
            tc.assertGet(CannotSeeTokens(), user)

    def test_post(self):
        class PostCheck(DCTestClientCheck):
            def __init__(self, token):
                self.token = token
            def get_default_data(self, tc):
                from django.core.files.base import ContentFile
                submission = [ {
                    "id": [ { "type": "login", "id": "enrico" } ],
                    "contributions": [ { "type": "tester" } ]
                }, ]
                return {"source": "test", "auth_token": self.token, "data": ContentFile(json.dumps(submission), "foo.json") }

        class CanPost(PostCheck):
            def check_result(self, tc):
                tc.fixture.assertEqual(tc.response.status_code, 200)
                data = tc.response.json()
                tc.fixture.assertEqual(data["code"], 200)
                tc.fixture.assertEqual(data["records_parsed"], 1)
                tc.fixture.assertEqual(data["contributions_processed"], 1)

        class CannotPost(PostCheck):
            def check_result(self, tc):
                tc.fixture.assertEqual(tc.response.status_code, 403)
                data = tc.response.json()
                tc.fixture.assertEqual(data["errors"][0], "Authentication token is not correct")
                tc.fixture.assertEqual(data["code"], 403)
                tc.fixture.assertEqual(data["records_parsed"], 0)
                tc.fixture.assertEqual(data["contributions_processed"], 0)

        tc = DCTestClient(self, 'contributors_post')

        # Only POST is allowed
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertGet(MethodNotAllowed(), user)

        # Posting with the right token is allowed by anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertPost(CanPost("testsecret"), user)

        # Posting with the wrong token is forbidden to anyone
        for user in (self.user_admin, self.user_dd, self.user_alioth, None):
            tc.assertPost(CannotPost("testsecret1"), user)

    def test_claim(self):
        tc = DCTestClient(self, 'contributors_claim')
        for user in (self.user_admin, self.user_dd):
            tc.assertGet(Success(), user)
        for user in (self.user_alioth, None):
            tc.assertGet(Forbidden(), user)

        for user in (self.user_admin, self.user_dd):
            tc.assertPost(Success(), user, data={"type": "email", "name": "enrico@enricozini.org", "person": "enrico@debian.org"})
        for user in (self.user_alioth, None):
            tc.assertPost(Forbidden(), user, data={"type": "email", "name": "enrico@enricozini.org", "person": "enrico@debian.org"})

    def test_claim_idents(self):
        tc = DCTestClient(self, 'contributors_claim_idents', url_kwargs={"type": "email"})
        for user in (self.user_admin, self.user_dd):
            tc.assertGet(Success(), user)
        for user in (self.user_alioth, None):
            tc.assertGet(Forbidden(), user)

    def test_claim_people(self):
        tc = DCTestClient(self, 'contributors_claim_people')
        for user in (self.user_admin, self.user_dd):
            tc.assertGet(Success(), user)
        for user in (self.user_alioth, None):
            tc.assertGet(Forbidden(), user)


class MIATestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1", "alioth", "alioth1", None:
            cls._add_method(cls._test_success, user)

    def _test_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_mia"))
            self.assertContains(response, "Debian Developers Missing In Action")


class MIAQueryTestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1":
            cls._add_method(cls._test_success, user)

        for user in "alioth", "alioth1", None:
            cls._add_method(cls._test_forbidden, user)

    def _test_success(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_mia_query"))
            self.assertContains(response, "Query contributors by identifier")

    def _test_forbidden(self, user):
        with self.login(user):
            response = self.client.get(reverse("contributors_mia_query"))
            self.assertPermissionDenied(response)


class APITestCase(SourceFixtureMixin, TestCase):
    @classmethod
    def setUpClass(cls):
        super(APITestCase, cls).setUpClass()
        cmodels.AggregatedPersonContribution.recompute()
        cmodels.AggregatedSource.recompute()
        cmodels.AggregatedPerson.recompute()

    @classmethod
    def __add_extra_tests__(cls):
        for user in "admin", "dd", "dd1":
            cls._add_method(cls._test_success, user)

        for user in "alioth", "alioth1", None:
            cls._add_method(cls._test_forbidden, user)

    def _test_success(self, user):
        client = self.make_test_apiclient(user)
        response = client.get(reverse('contributors-list'), format="html")
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["results"]), 5)

    def _test_forbidden(self, user):
        client = self.make_test_apiclient(user)
        response = client.get(reverse('contributors-list'))
        self.assertPermissionDenied(response)
